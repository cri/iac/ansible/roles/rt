#!/usr/bin/env python3

import os
import psycopg2
from sys import stdout, stderr
from subprocess import call

args = {
    "dbname": "{{ rt_db_name }}",
    "user": "{{ rt_db_user }}",
    "password": "{{ rt_db_password }}",
    "host": "{{ rt_db_host }}",
    "port": "{{ rt_db_port }}",
}

LOCAL_TEMPLATES = {
    True: (
        'rt-{0}  "|/usr/bin/procmail -a {0} /etc/postfix/davenull.procmail"\n',
        'rt-{0}-comment  "|/usr/bin/rt-mailgate --queue {0} --action comment --url http://localhost\n',
    ),
    False: (
        'rt-{0}  "|/usr/bin/rt-mailgate --queue {0} --action correspond --url http://localhost"\n',
        'rt-{0}-comment  "|/usr/bin/rt-mailgate --queue {0} --action comment --url http://localhost\n',
    ),
}

VIRTUAL_TEMPLATES = (
    "{1}  rt-{0}\n",
    "{2}  rt-{0}-comment\n",
)

RT_POSTFIX_LOCAL_ALIAS_PATH = "{{ rt_postfix_local_alias_path }}"
RT_POSTFIX_VIRTUAL_ALIAS_PATH = "{{ rt_postfix_virtual_alias_path }}"

# Check that path exists
for p in (RT_POSTFIX_LOCAL_ALIAS_PATH, RT_POSTFIX_VIRTUAL_ALIAS_PATH):
    os.makedirs(os.path.dirname(p), exist_ok=True)
    if not os.path.exists(p):
        with open(p, "w") as f:
            f.write("")

with psycopg2.connect(**args) as conn:
    with conn.cursor() as cur:
        cur.execute(
            "SELECT queues.name "
            "FROM queues "
            "JOIN objectcustomfieldvalues ON objectid = queues.id AND content = 'On' "
            "JOIN customfields ON objectcustomfieldvalues.customfield = customfields.id "
            "WHERE customfields.name = 'davenull' AND queues.disabled = 0 AND objectcustomfieldvalues.disabled = 0;"
        )

        davenull_queues = [r[0] for r in cur]

        cur.execute(
            "SELECT name, correspondaddress, commentaddress "
            "FROM public.queues "
            "WHERE disabled = 0;"
        )

        with open(RT_POSTFIX_LOCAL_ALIAS_PATH, "w") as fl, open(
            RT_POSTFIX_VIRTUAL_ALIAS_PATH, "w"
        ) as fv:
            for record in cur:
                for t in LOCAL_TEMPLATES[record[0] in davenull_queues]:
                    fl.write(t.format(*record))
                for t in VIRTUAL_TEMPLATES:
                    fv.write(t.format(*record))

for p in (RT_POSTFIX_LOCAL_ALIAS_PATH, RT_POSTFIX_VIRTUAL_ALIAS_PATH):
    call(["postmap", p], stdout=stdout, stderr=stderr)
