#!/usr/bin/env perl
#=============================================================================
#
#         FILE: davenull.pl
#
#        USAGE: ./davenull.pl [options] < MAIL
#
#  DESCRIPTION: Detects if the given email follows the EPITA netiquette and
#               emits a message listing the broken rules (if any).
#
#      OPTIONS: --brutal: stops immediatly after the first error found
#               --debug: instead of sending a message, print it on stdin
#               --always-answer: answer on acceptance too
# REQUIREMENTS: Email::MIME, Email::Sender::Simple
#         BUGS: none known
#       AUTHOR: Thibaut Le Page <kalite@acu.epita.fr>
# ORGANIZATION: EPITA
#      CREATED: 07/25/2013 03:16:00 PM
#=============================================================================

use v5.14;
use strict;
use warnings;
use utf8;
use open IO => ':utf8';
use open ':std';

our $VERSION = 2.25;

use Encode;                               # core
use Getopt::Long;                         # core
use List::Util qw(reduce);                # core
use MIME::Base64;                         # core
use Pod::Usage;                           # core
use Sys::Syslog qw(:standard :macros);    # core
use Text::Tabs;                           # core
use Text::Wrap;                           # core
use Email::MIME 1.910;
use Email::Sender::Simple 0.110005 qw(sendmail);

our ($a, $b); # to get rid of useless warnings concerning 'reduce'

openlog( 'davenull', 'pid', LOG_MAIL );

### CONSTANT PARAMETERS ######################################################

use constant NETIQUETTE_URL => 'https://static.assistants.epita.fr/netiquette.pdf';
use constant EMAIL_VALID => 1;    # 1 is the procmail value for True
use constant { # tags
    WARNING => '[warning]',
    ERROR   => '[error]',
};
my $SIGBLOCK = <<'EOF';
--
Dave
EOF

### COMMAND-LINE #############################################################

our $BRUTAL        = 0;
our $DEBUG         = 0;
our $EVIL          = 0;
our $ALWAYS_ANSWER = 0;
our @CCs;
our @BCCs;
my $help = 0;
GetOptions(
    'brutal'        => \$BRUTAL,
    'evil'          => \$EVIL,
    'always-answer' => \$ALWAYS_ANSWER,
    'debug'         => \$DEBUG,
    'cc=s'          => \@CCs,
    'bcc=s'         => \@BCCs,
    'help'          => \$help,
) or pod2usage(2);

pod2usage( -exitval => 0, -verbose => 2 ) if $help;

### GLOBALS ##################################################################

our @ERRORS;
our $MSG;
our $MSG_ORIG;
our $MSG_BODY;
our $MSG_SUBJECT = '(unknown)';     # subject of the analyzed email
our $MSG_FROM    = '(unknown)';     # sender of the analyzed email
our $MSG_TARGET  = 'ASSISTANTS';    # YAKA/ACU
$Text::Wrap::columns = 72;
$Text::Wrap::break   = qr/ \s (?! [!?:;»] ) /x;
my $DEBUG_BLOCKS;

### FUNCTIONS ################################################################

sub exists_errors {
    return grep { $_->[2] eq ERROR } @ERRORS;
}

sub answer_invalid_evil {
    my $original_subject = shift // '';

    my @standard_blocks = grep { @$_ > 1 } @{ $DEBUG_BLOCKS->{standard} };
    @standard_blocks = @{ $DEBUG_BLOCKS->{standard} } unless @standard_blocks;
    my $block = $standard_blocks[int(rand(@standard_blocks))];
    my $str = "$MSG_FROM wrote:\n";
    for my $l (@$block) {
        $str .= '> ' . $l->{text} . "\n";
    }
    return $str . <<"EOF";
\nhaha cool

but your message isn't netiquette-compliant
unfortunately

--
DarkDave
EOF
}

sub answer_invalid {
    goto &answer_invalid_evil if $EVIL;
    my $original_subject = shift // '(dave: no subject)';
    my $errors = shift // [];
    my $netiquette_url = NETIQUETTE_URL;
    my $display_errors;
    if ($BRUTAL) {
        my @errors = grep { $_->[2] eq ERROR } @$errors;
        my $error = '« ' . $errors[0]->[0] . ' »' . $errors[0]->[1];
        $display_errors =
          "En particulier, voici la première erreur relevée : $error.";
    }
    else {
        my $list =
          join "\n\n" => map { '  - ' . $_->[0] . $_->[1] . ' ' . $_->[2] . ' ;' }
          @$errors;
        $list =~ s/\s+;$/./;
        $display_errors =
            "Les erreurs suivantes ont notamment été relevées (en cas "
          . "d'occurrences multiples, seule la première est signalée) :"
          . "\n\n$list";
    }
    my $text = <<"EOF";
Bonjour,

Votre courriel, intitulé « $original_subject », a été rejeté car il ne respecte pas la nétiquette [1], indispensable pour contacter les assistants.

$display_errors

Merci de consulter la nétiquette avant de renvoyer votre courriel, afin d'éviter un nouveau rejet. Si ce problème persiste sans raison apparente, vous pouvez passer au lab.

[1] : $netiquette_url

Cordialement,

$SIGBLOCK
EOF
    return $text;
}

sub answer_valid {
    my ( $original_subject ) = @_;
    my $netiquette_url = NETIQUETTE_URL;
    my $text = <<"EOF";
Bonjour,

Votre courriel, intitulé « $original_subject », semble respecter la nétiquette [1] ou, du moins, ce qui peut en être vérifié automatiquement.

Si vous estimez qu'il aurait pourtant dû être rejeté, merci d'envoyer un ticket décrivant le problème à <qualite\@acu.epita.fr> (y attacher votre message d'origine en facilitera la correction).

[1] : $netiquette_url

Bien cordialement,

$SIGBLOCK
EOF
    return $text;
}

sub send_mail {
    my $msg = shift;
    sendmail($msg, { to => [ $MSG_FROM, @CCs, @BCCs ] })
}

sub build_email {
    my ($subject, $content) = @_;
    my $mail = Email::MIME->create(
        header_str => [
            From     => '/dev/null <dave@acu.epita.fr>',
            To       => $MSG_FROM,
            Subject  => "[$MSG_TARGET][NETIQUETTE] $subject",
            'X-Loop' => 'davenull@acu.epita.fr',
            Cc       => join( ', ' => @CCs ),
          ],
        attributes => {
            encoding => 'quoted-printable',
            charset  => 'utf-8',
            format   => 'flowed',
        },
        body_str => $content,
    );
    return $mail;
}

sub print_debug {
    my $answer = shift;
    $answer = $answer->as_string;
    my $blocks = debug_blocks($DEBUG_BLOCKS);
    my $header = "Dave Null v$VERSION, sortie complète de débogage";
    my $date = localtime;
    chomp(my $machine = `uname -a` || 'unknown host');

    print STDERR <<"EOF"
@{[ '=' x length($header) ]}
$header
@{[ '=' x length($header) ]}

Date: $date
Host: $machine
Perl: v$]
External modules:
    Email::MIME: . . . . . . v$Email::MIME::VERSION
    Email::Sender::Simple: . v$Email::Sender::Simple::VERSION

BLOCS
=====

$blocks

MESSAGE DE RÉPONSE
==================

$answer
(Fin de la sortie de débogage.)
@{[ '=' x length($header) ]}
EOF
}

sub reject {
    my $answer =
      build_email( 'Courriel rejeté',
        answer_invalid( $MSG_SUBJECT, \@ERRORS ) );
    if   ($DEBUG) { print_debug($answer) }
    else          { send_mail($answer) }
    writelog( 0, $MSG_FROM, $MSG_SUBJECT,
        eval { $ERRORS[0]->[0] } // '(unknown problem!)' );
    exit int( !EMAIL_VALID );
}

sub ok {
    if ($ALWAYS_ANSWER) {
        my $answer =
          build_email( 'Courriel accepté', answer_valid($MSG_SUBJECT) );
        if   ($DEBUG) { print_debug($answer) }
        else          { send_mail($answer) }
    }
    writelog( 1, $MSG_FROM, $MSG_SUBJECT );
    exit int(EMAIL_VALID);
}

sub register_error {
    my ( $msg, $line, $type ) = @_;
    $line = defined $line ? " (l. $line)" : '';
    push @ERRORS, [ $msg, $line, $type ];
    return;
}

# Store a new error. If in brutal mode, stops the analysis.
sub error ($;$) { ## no critic ProhibitSubroutinePrototypes
    my ( $err, $line ) = @_;
    register_error($err, $line, ERROR);
    reject() if $BRUTAL;
}

sub warning ($;$) { ## no critic ProhibitSubroutinePrototypes
    my ( $err, $line ) = @_;
    register_error($err, $line, WARNING);
}

sub failed ($) { ## no critic ProhibitSubroutinePrototypes
    syslog( LOG_ERR, $_[0] );
    return 1;
}

sub writelog {
    my ($ok, $sender, $subject, $reason) = @_;
    syslog(
        $ok ? LOG_NOTICE : LOG_INFO,
        "%s from %s: %s: %s",
        $ok ? 'authorized' : 'refused',
        $sender,
        $subject,
        $reason // '(clean)'
    );
}

# Pretty-print the email body structure (standard text, quotations,
# signatures) as detected by Dave. Does not have any effect without --debug.
sub debug_blocks {
    my $blocks = shift;
    my $out = '';
    for my $type (qw< standard quoting signature >) {
        $out .= uc($type)." BLOCKS:\n";
        my $i = 1;
        for ( @{ $blocks->{$type} } ) {
            $out .= "\t----- BLOCK $i -----\n";
            for my $l (@$_) {
                $out .= sprintf "\t\tl. %-4d %s\n", $l->{line}, $l->{text};
            }
        }
        continue { $i += 1 }
    }
    return $out;
}

# Returns 1 if both arguments are tags in the same "clever" spirit than
# "[BALISE1][BALISE2]"; returns 0 otherwise.
sub smartass_tags {
    my ( $a, $b ) = @_;

    # Same tags
    if ( $a eq $b ) { return 1 }

    # Numbering
    elsif ( $a =~ / ([0-9]+) $ /x && $b =~ / \Q@{[ $1 + 1 ]}\E $ /x ) {
        return 1;
    }

    # Numbering in an arbitrary base
    elsif ( $b =~ / \Q@{[ ++(my $tmp = substr($a, -1)) ]}\E $ /x ) {
        return 1;
    }

    # Numbering using first roman numerals
    elsif ($a =~ / ([i1])(\1*) $ /ix
        && $b =~ / @{[ $1 x (length($2) + 2) ]} $ /x )
    {
        return 1;
    }

    return 0;
}

### DAVE NULL HIMSELF ########################################################

# Get the mail
{
    local $/;
    $MSG = <STDIN>;
}
die "$0: error: empty STDIN (email expected)\n" unless $MSG;
$MSG         = encode_utf8($MSG);
$MSG_ORIG    = $MSG;
$MSG =~ s/(?<!\r)\n/\r\n/g;
$MSG         = Email::MIME->new($MSG);
$MSG_SUBJECT = $MSG->header('Subject') || '(dave: no subject)';
$MSG_FROM    = $MSG->header('From') || '(dave: no sender)';
if ( ( $MSG->header('To') || '' ) =~
    m/ (?| \b(yaka|acu)\@epita\.fr\b | \@(yaka|acu)\.epita\.fr\b ) /x )
{
    $MSG_TARGET = uc $1;
}

# Do NOT read $MSG_SUBJECT nor $MSG_FROM before this line!

# Whitelist/blacklist
WHITELIST: {
    last WHITELIST unless defined $ENV{DAVENULL_WHITELISTS};
    my ($sender) = $MSG_FROM =~ / ([^<\s]+\@[^>\s]+) /x;
    for my $file ( split /:/ => $ENV{DAVENULL_WHITELISTS} ) {
        open my $fh, '<', $file
          or failed "Can't open whitelist file $file: $!" && next;
        while (<$fh>) {
            chomp;
            if ( $_ eq $sender ) {
                writelog( 1, $MSG_FROM, $MSG_SUBJECT,
                    "whitelisted sender <$sender>" );
                exit int(EMAIL_VALID);
            }
        }
    }
}

# Mail must be either text/plain or multipart/mixed with text/plain as first
# subpart.
my ($ct) = ($MSG->content_type || '') =~ /^([^;]+)/;
$ct ||= '';
if ( $ct ne 'text/plain' ) {
    if ( $ct ne 'multipart/mixed' ) {
        my $err = "Content-Type '$ct' interdit";
        $err .= ' (les autres tests pourraient être faussés)' unless $BRUTAL;
        error $err;

        # We want the subparts extracted even if not multipart/mixed:
        $ct = 'multipart/mixed' if $ct eq 'multipart/alternative';
    }
    if ( $ct eq 'multipart/mixed' ) {
        $MSG_BODY = @{ [ $MSG->subparts ] }[0] || '';
        my ($sct) = ($MSG_BODY->content_type || '') =~ m{^([^;]+)};
        error "la première sous-partie du message devrait être en text/plain "
          . "(plutôt qu'en '$sct')"
          unless $sct eq 'text/plain';
        $MSG_BODY = $MSG_BODY->body_str || '';
    }
}

# Well-formed subject?
{
    my $rM     = qr/ (?: (?:Re:|Fwd:) \s? ) /xi;
    my $rTagML = qr/ ML- [^\]]+ /x;
    my $rTag   = qr/ [^\]]+ /x;
    my $rSp    = qr/ /;
    my $rLogin = qr/[a-z][-a-z0-9]{1,5}_[a-z0-9]/;
    $MSG_SUBJECT =~ s/ \[ \S+ \  \# \d+ \] \h* //x;
    if ( $MSG_SUBJECT =~ /
        ^ $rM? (?: \[ $rTagML \] \ ? )?
        \[ (?<tag1>$rTag) \]
        \[ (?<tag2>$rTag) \]
        $rSp
        (?: $rLogin $rSp - $rSp )?
        (?<subj>.+)
        $/x )
    {
        my ( $tag1, $tag2, $obj ) = ( $+{tag1}, $+{tag2}, $+{subj} );
        error 'sujet trop long' if length $MSG_SUBJECT > 72;
        error 'les balises doivent être capitalisées'
          unless $tag1 . $tag2 eq uc $tag1 . $tag2;
        error '[MISC] n\'est autorisé qu\'en première balise'
          if $tag2 eq 'MISC';
        error 'balise-id = 1*( UPPER / DIGIT / "-" / "_" / "+" / "/" )'
          unless ($tag1 . $tag2) =~ m|^[A-Z0-9_+/-]+$|;
        error "balise [$tag1] mal placée"  if $obj =~ / \[ $rTag \] /x;
        error "balise [$tag1] trop longue" if length $tag1 > 10;
        error "balise [$tag2] trop longue" if length $tag2 > 10;

        # Try to detect attempts to bypass the fact that Dave forces the use
        # of two tags. Users generally try something like [BALISE1][BALISE2],
        # but we must expect smarter with the avent of this very feature.
        if (smartass_tags($tag1, $tag2)) {
            error "$tag1/$tag2 ne correspond pas à thème/problématique";
        }
    }
    else {
        error 'tête de sujet ("[BALISE1][BALISE2] Sujet") mal formatée';
    }
}

eval { $MSG_BODY //= $MSG->body_str || '' } or exists_errors() ? reject() : ok();

# Body format
{
    last if ($MSG->content_type || '') =~ /\bformat=("?)flowed\1/;
    my @lines = map { expand; chomp; $_ } split /\n/ => $MSG_BODY;
    for my $i ( 1 .. @lines ) {

        # lines without blanks
        next unless $lines[ $i - 1 ] =~ /\h/;

        # standard lines
        if ( $lines[ $i - 1 ] =~ /^>/ ) {    # quote: 80 columns
            if ( length $lines[ $i - 1 ] > 81 ) {    # don't forget \n!
                error( 'au moins une ligne de citation dépasse 80 colonnes', $i );
                last;
            }
        }
        else {
            if ( length $lines[ $i - 1 ] > 73 ) {    # not quote => 72 columns
                warning( 'au moins une ligne standard dépasse 72 colonnes', $i );
            }
            if ( length $lines[ $i - 1 ] > 81 ) {    # not quote => 81 columns
                error( 'au moins une ligne standard dépasse 80 colonnes', $i );
                last;
            }
        }
    }
}

# Top-post, etc.
{
    # be careful to use chop() instead of chomp() here
    my @lines = map { expand; chop; $_ } split /\n/ => $MSG_BODY;

    my %blocks = (
        standard  => [],
        quoting   => [],
        signature => [],
    );
    my %err;
    my $s = 'standard';    # final state
    {
        my $previous_s = $s;    # copy of the previous line's state
        my $l          = 1;     # current line
        my $block = [];         # current block buffer
        my $sig_buffer = [];    # a maybe-(illformed)-signature
        my $sig_first;     # number of the first line recorded by @sig_buffer

        # States
        while ( defined($_ = shift @lines) ) {

            # sigfail detection
            if (   !@$sig_buffer
                && ( $sig_first // 0 ) != $l
                && $s ne 'signature'
                && /^--(?:\s{2,}|\t)?$/ )
            {
                $sig_first = $l;
                push @{$sig_buffer}, { text => $_, line => $l };
            }
            elsif (@$sig_buffer) {
                if (/^--\s*$/) {    # eventually it seems it wasn't a sigfail
                    unshift @lines, @$sig_buffer, $_;
                    $sig_buffer = [];
                    $l          = $sig_first - 1;
                }
                elsif (/^(> ?)\1* ?[^>]/) {    # probably a top-post
                    unshift @lines, $_;
                    push @{$blocks{signature}}, $sig_buffer;
                    $err{emulated_sig} ||= $sig_first;
                    $sig_buffer = [];
                    $l -= 1;
                    $s = 'signature';
                }
                else { push @{$sig_buffer}, { text => $_, line => $l } }
            }

            # don't seem to be a sigfail
            else {

                if ( $s eq 'standard' ) {
                    if    (/^$/) { $s = 'intermediate' }
                    elsif (/^>/) { $s = 'quoting' }
                    elsif (/^-- $/) {
                        $err{sig_nolf} ||= $l;
                        $s = 'signature';
                    }
                }
                elsif ( $s eq 'intermediate' ) {
                    if    (/^>/)    { $s = 'quoting' }
                    elsif (/^-- $/) { $s = 'signature' }
                    elsif (/^$/) { $err{lflf} ||= $l }
                    else         { $s = 'standard' }
                }
                elsif ( $s eq 'quoting' ) {
                    if (/^$/) { $s = 'intermediate' }
                    elsif (/^-- $/) {
                        $err{sig_nolf} ||= $l;
                        $s = 'signature';
                    }
                    elsif (/^[^>]/) {
                        $err{quot_nolf_after} ||= $l;
                        $s = 'standard';
                    }
                }
                elsif ( $s eq 'signature' ) {    # top-post detection
                    if (/^(> ?)\1* ?[^>]/) { $s = 'quoting' }
                }

                unless ( $s eq $previous_s ) {
                    push @{$blocks{$previous_s}}, $block
                      if $previous_s ne 'intermediate';
                    $block = [];
                }
                push @{$block}, { text => $_, line => $l };
                $previous_s = $s;
            }
        }
        continue { $l += 1 }

        # Push the last block
        push @{$blocks{$previous_s}}, $block if $previous_s ne 'intermediate';

        # If there is something in @sig_buffer at the end, use it as a signature
        if (@$sig_buffer) {
            push @{$blocks{signature}}, $sig_buffer;
            $err{emulated_sig} ||= $sig_first;
            $s = 'signature';
        }
    }

    # Clean a superfluous CRLF below sig block
    if ( @{ $blocks{signature} } && $blocks{signature}->[0][-1]{text} eq '' ) {
        pop @{$blocks{signature}->[0]};
    }

    $DEBUG_BLOCKS = \%blocks;

    {    # Check the previously delimited blocks

      SIGNATURE:
        {
            error 'signature oubliée ou méconnaissable' if $s ne 'signature';
            last SIGNATURE unless @{ $blocks{signature} };

            my $signature = $blocks{signature}->[0];
            my $intro     = shift @{$signature};

            error 'signature de plus de 4 lignes' if @$signature > 4;
            error 'un saut de ligne doit précéder la signature',
              $err{sig_nolf}
              if $err{sig_nolf};

            if ( $intro->{text} =~ /^--$/ ) {
                error 'signature incorrecte '
                  . '(espace manquante après "--")', $intro->{line};
            }
            elsif ( $intro->{text} !~ /^-- $/ ) {
                error 'signature incorrecte '
                  . '(première ligne différente de "-- ")', $intro->{line};
            }
        }

      QUOTATIONS:
        {
            last QUOTATIONS unless @{$blocks{quoting}};
            my @all_lines_quoted = do {
                my @t = @{ $blocks{quoting} };
                if ( @t == 1 ) { @{$t[0]} }
                else {
                    my $res = reduce { [ @$a, @$b ] } @t;
                    @$res;
                }
            };

            if ( my ($line) =
                grep { $_->{text} =~ /^(> ?)\1* --\s*$/ } @all_lines_quoted )
            {
                error 'signature citée', $line->{line};
            }

            if ( $err{quot_nolf_after} ) {
                my $block_first_line = do {
                    my $l = 0;
                    my $fl;
                    for ( my $i = 0 ; $i <= $#{ $blocks{quoting} } ; $i += 1 ) {
                        $l = $blocks{quoting}->[$i][0]{line};
                        last if $l > $err{quot_nolf_after};
                        $fl = $blocks{quoting}->[$i][0]{text};
                    }
                    substr( $fl, 0, 30 );
                };
                error "un saut de ligne doit suivre le bloc de citation qui "
                  . "commence par « $block_first_line »",
                  $err{quot_nolf_after};
            }

            for (@all_lines_quoted) {
                if ( $_->{text} !~ /^ >+ (?: \h* $ | \h* (?!>) ) /x ) {
                    error "les chevrons introductifs d'un bloc de citation "
                      . "doivent être consécutifs", $_->{line};
                    last;
                }
            }
            for (@all_lines_quoted) {
                if ( $_->{text} =~ / ^ > [>\h]* (?<! \h ) (?! $ | \  | > ) /x ) {
                    error "les chevrons introductifs d'un bloc de citation "
                      . "doivent être suivis d'une espace", $_->{line};
                    last;
                }
            }
        }

      TOP_POST:
        {
            last TOP_POST unless @{ $blocks{quoting} };
            error 'top-post détecté'
              if $blocks{quoting}->[0][0]{line} >
              $blocks{standard}->[-1][-1]{line};
        }

        # OTHERS:
        warning 'ne séparez vos paragraphes que par un saut de ligne', $err{lflf}
          if $err{lflf};

    }

}

### NO MORE TESTS AFTER THIS LINE ############################################

if   ( exists_errors() ) { reject() }
else                     { ok() }

END {
    closelog();
}

__END__

=head1 NAME

Dave Null - The netiquette's guardian angel

=head1 USAGE

    ./davenull.pl [OPTIONS]  <FILE

=head1 DESCRIPTION

Dave Null reads the email given on its standard input and checks if it
respects the EPITA netiquette. When running without any option that alters its
behaviour, if the email is not netiquette-compliant, Dave emails the sender
and returns a value considered as "false" by Procmail, thus redirecting the
original email to /dev/null.

=head2 Syslog

Dave makes calls to syslogd (through Sys::Syslog) in the following cases:

=over

=item

an email is accepted;

=item

an email is rejected;

=item

one of the whitelists can't be read.

=back

=head2 Whitelisting

Dave can use whitelists to always authorize emails from a list of trusted
senders. For each analyzed email, before any netiquette-related check, Dave
read the DAVENULL_WHITELISTS environment variable, which must be a
colon-separated list of paths. These paths must lead to files composed of an
email address on each line. If one of these address is the address of the
current email sender, then the mail is immediatly accepted, without any
further check.

=head1 REQUIRED ARGUMENTS

Dave reads its standard input until EOF and analyzes it.

=head1 OPTIONS

=over

=item C<--always-answer>

Dave emits a confirmation if the email is valid regarding to the netiquette.

=item C<--brutal>

If a broken rule is found, Dave does not search for other infractions.

=item C<--debug>

Instead of sending an email, Dave prints its message on stderr, plus its
analysis of the email's structure. (Supports C<--always-answer>.)

=item C<--evil>

The rejection email Dave sends does not list detected infractions but instead
just reply "haha cool" regardless of sender or message content.

=item C<--cc>

If a mail is sent back by Dave (be it rejection, or acceptance with
C<--always-answer>), it will be cc'ed to each address given to this option.
You can specify multiple C<--cc> clauses.

=item C<--bcc>

Same as C<--cc> but with the C<Bcc> header.

=item C<--help>

Displays the help.

=back

=head1 EXIT STATUS

Let the EMAIL_VALID value, defined at the top of davenull.pl.

If the email is considered valid, Dave exits with EMAIL_VALID. Otherwise, it
exits with !EMAIL_VALID.

=head1 DEPENDENCIES

=over

=item

perl >= 5.14 (although it would be easy to allow 5.10.1)

=item

Email::MIME v1.910+

=item

Email::Sender::Simple v0.110005+

=back

=head1 AUTHOR

Thibaut Le Page <kalite@acu.epita.fr>

=head1 LICENSE AND COPYRIGHT

B<The MIT License (MIT)>

Copyright (c) 2013 Thibaut Le Page

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
