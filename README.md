# Ansible role for RT

This repository contains an Ansible role to install and configure RT.

## Notes on usage

Installation of perl dependencies can take a very long time.

It appears that the role can break :
- If the installation process of dependencies is stopped, it cannot be resumed easily (e.g. full reinstallation of system)
